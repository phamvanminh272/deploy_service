from flask import Flask
from predict_app import Predictor
from flask_restplus import Resource, Api
from datetime import datetime
import json
import sys
app = Flask(__name__)
api = Api(app)
src_vocab_path = "checkpoint/vocab.src"
tgt_vocab_path = "checkpoint/vocab.tgt"
model_path = "checkpoint/aivivn_tone.model.ep25"
wlm_path = "lm/corpus-wplm-4g-v2.binary"
predictor = Predictor(src_vocab_path, tgt_vocab_path, model_path, wlm_path)
@api.route('/spell/<string:text>')
class spell(Resource):
    def get(self, text):
        start = datetime.now()
        test_string = "data/test.txt"
        test_cleaned_string = "data/test_cleaned.txt"
        out_path = "data/submission.txt"
        result = predictor.predict(text, text, out_path)
        # result = result.encode('utf-8')
        duration = datetime.now() - start
        dic = {'duration': str(duration),'result': result}
        print(dic)
        return dic

if __name__ == '__main__':
    # reload(sys)
    # sys.setdefaultencoding('utf-8')
    app.run(debug=True, port=5000)